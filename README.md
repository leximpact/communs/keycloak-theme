# Keycloak theme for Leximpact

See https://www.keycloak.org/docs/latest/server_development/#_themes for instructions on how to create and maintain a Keycloak theme.

HTML templates are based on [Freemarker Templates](https://freemarker.apache.org/).
