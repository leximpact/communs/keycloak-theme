// cf https://stackoverflow.com/questions/8100576/how-to-check-if-dom-is-ready-without-a-framework
var domPatched = false;

if (document.readyState === "complete") {
  // Fully loaded!
  patchDom();
} else if (document.readyState === "interactive") {
  // DOM ready! Images, frames, and other subresources are still downloading.
  patchDom();
} else {
  // Loading still in progress.
  // To wait for it to complete, add "DOMContentLoaded" or "load" listeners.

  window.addEventListener("DOMContentLoaded", () => {
    // DOM ready! Images, frames, and other subresources are still downloading.
    patchDom();
  });

  window.addEventListener("load", () => {
    // Fully loaded!
    if (!domPatched) {
      patchDom();
    }
  });
}

function patchDom() {
  domPatched = true;
  theParent = document.getElementById("kc-form");
  theKid = document.createElement("button");
  theKid.innerHTML = "Autre compte";
  theKid.classList.add("alt-login");

  // append theKid to the end of theParent
  theParent.appendChild(theKid);

  // prepend theKid to the beginning of theParent
  theParent.insertBefore(theKid, theParent.firstChild);

  const AltLogin = document.querySelector("#kc-form-login");
  theKid.onclick = () => AltLogin.classList.toggle("active");
}
